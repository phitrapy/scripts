#!/bin/bash


# ℹ️ Infos:
# * La branche cible doit être connue en local
# * Dans cette version, aucun traitement n'est effectué sur le repo origin par défaut

# Conf Recommandée:
# git config --global rebase.autoskip true


for arg in "$@"; do
    case $arg in
        --push) # ⚠️ Vérifier la stabilité du script avant d'activer cette option
            push_commits=true
            ;;
    esac
done

# Define the branches
base_branch="$(git name-rev --name-only HEAD)"
target_branch="$1"

if [[ -z "$target_branch" ]]; then
  echo "Erreur: pas de branche cible"
  exit 1;
fi

# Get the list of commits between the branches
commits="$(git rev-list ${base_branch} ^${target_branch})"
branches=""

for commit in $commits; do
  branch=$(git name-rev --name-only $commit)
  echo $commit "->" $branch
  branches="${branch} ${branches}"
done


# Stash changes before doing anything
git add .
git stash save "Stash before mass rebase of $base_branch onto $target_branch"

# Start the rebase
current_branch=""
for branch in $branches; do
  current_branch="$branch";
  git checkout $current_branch;
  git rebase $target_branch;
  if [[ -n "$push_commits" ]]; then
     git push --force origin $current_branch
  fi
  current_branch="$target_branch";
done

